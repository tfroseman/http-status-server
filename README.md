# HTTP Status Server

Often times I need to generate specific status codes from backend services for analytic data and error handling.
There are other services that provide this online but if you need to self host this can be used as a single binary
locally.

# How to Start

## Build from src

This will write out the executable to a file named "response-code-servers". If you want a different file name replace
"response-code-server" with the desired executable name.

$ `go build -o response-code-server`

## Start the server

$ `./response-code-server`

# Deployment

## Simple

nohup ./response-code-server &

## System Service

For long running services using systemctl to control service crashes or start on system boot would be the best option.

## Web Server

It is possible to use the golang binary as the fronted webserver but it would be best to deploy behind a proxy like
nginx or apache.


