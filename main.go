package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Starting server on localhost:8080")

	http.HandleFunc("/", requestHandler)

	log.Fatal(http.ListenAndServe(":8080", nil))
}

func requestHandler(w http.ResponseWriter, request *http.Request){
	switch request.URL.Path[1:] {
	case "100":
		w.WriteHeader(http.StatusContinue)
		w.Write([]byte("100 - Continue"))
	case "101":
		w.WriteHeader(http.StatusSwitchingProtocols)
		w.Write([]byte("101 - Switching Protocols"))
	case "102":
		w.WriteHeader(http.StatusProcessing)
		w.Write([]byte("102 - Switching Protocols"))

	case "200":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("200 - OK"))
	case "201":
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte("201 - Created"))
	case "202":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte("202 - Accepted"))
	case "203":
		w.WriteHeader(http.StatusNonAuthoritativeInfo)
		w.Write([]byte("203 - Non-Authoritative Info"))
	case "204":
		w.WriteHeader(http.StatusNoContent)
		w.Write([]byte("204 - No Content"))
	case "205":
		w.WriteHeader(http.StatusResetContent)
		w.Write([]byte("205 - ResetContent"))
	case "206":
		w.WriteHeader(http.StatusPartialContent)
		w.Write([]byte("206 - Partial Content"))
	case "207":
		w.WriteHeader(http.StatusMultiStatus)
		w.Write([]byte("207 - Multi-Status"))
	case "208":
		w.WriteHeader(http.StatusAlreadyReported)
		w.Write([]byte("208 - Already Reported"))
	case "226":
		w.WriteHeader(http.StatusIMUsed)
		w.Write([]byte("226 - IM Used"))

	case "300":
		w.WriteHeader(http.StatusMultipleChoices)
		w.Write([]byte("300 - Multiple Choices"))
	case "301":
		w.WriteHeader(http.StatusMovedPermanently)
		w.Write([]byte("301 - Moved Permanently"))
	case "302":
		w.WriteHeader(http.StatusFound)
		w.Write([]byte("302 - Found"))
	case "303":
		w.WriteHeader(http.StatusSeeOther)
		w.Write([]byte("303 - See Other"))
	case "304":
		w.WriteHeader(http.StatusNotModified)
		w.Write([]byte("304 - Not Modified"))
	case "305":
		w.WriteHeader(http.StatusUseProxy)
		w.Write([]byte("305 - Use Proxy"))
	case "307":
		w.WriteHeader(http.StatusTemporaryRedirect)
		w.Write([]byte("307 - Temporary Redirect"))
	case "308":
		w.WriteHeader(http.StatusPermanentRedirect)
		w.Write([]byte("308 - Permanent Redirect"))

	case "400":
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400 - Bad Request"))
	case "401":
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("401 - Unauthorized"))
	case "402":
		w.WriteHeader(http.StatusPaymentRequired)
		w.Write([]byte("402 - Payment Required"))
	case "403":
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("403 - Forbidden"))
	case "404":
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("404 - Not Found"))
	case "405":
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("405 - Method Not Allowed"))
	case "406":
		w.WriteHeader(http.StatusNotAcceptable)
		w.Write([]byte("406 - Not Acceptable"))
	case "407":
		w.WriteHeader(http.StatusProxyAuthRequired)
		w.Write([]byte("407 - Proxy Auth Required"))
	case "408":
		w.WriteHeader(http.StatusRequestTimeout)
		w.Write([]byte("408 - Request Timeout"))
	case "410":
		w.WriteHeader(http.StatusGone)
		w.Write([]byte("410 - Gone"))
	case "411":
		w.WriteHeader(http.StatusLengthRequired)
		w.Write([]byte("411 - Length Required"))
	case "412":
		w.WriteHeader(http.StatusPreconditionFailed)
		w.Write([]byte("412 - Precondition Failed"))
	case "413":
		w.WriteHeader(http.StatusRequestEntityTooLarge)
		w.Write([]byte("413 - Payload Too Large"))
	case "414":
		w.WriteHeader(http.StatusRequestURITooLong)
		w.Write([]byte("414 - URI too Long"))
	case "415":
		w.WriteHeader(http.StatusUnsupportedMediaType)
		w.Write([]byte("415 - Unsupported Media Type"))
	case "416":
		w.WriteHeader(http.StatusRequestedRangeNotSatisfiable)
		w.Write([]byte("416 - Request Range not Satisfiable"))
	case "417":
		w.WriteHeader(http.StatusExpectationFailed)
		w.Write([]byte("417 - Expectation Failed"))
	case "418":
		w.WriteHeader(http.StatusTeapot)
		w.Write([]byte("418 - I am a teapot"))
	case "421":
		w.WriteHeader(http.StatusMisdirectedRequest)
		w.Write([]byte("421 - Misdirected Request"))
	case "422":
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte("422 - Unprocessable Entity"))
	case "423":
		w.WriteHeader(http.StatusLocked)
		w.Write([]byte("423 - Locked"))
	case "424":
		w.WriteHeader(http.StatusFailedDependency)
		w.Write([]byte("424 - Failed Dependency"))
	case "426":
		w.WriteHeader(http.StatusUpgradeRequired)
		w.Write([]byte("426 - Upgrade Required"))
	case "428":
		w.WriteHeader(http.StatusPreconditionRequired)
		w.Write([]byte("428 - Precondition Required"))
	case "429":
		w.WriteHeader(http.StatusTooManyRequests)
		w.Write([]byte("429 - Too Many Requests"))
	case "431":
		w.WriteHeader(http.StatusRequestHeaderFieldsTooLarge)
		w.Write([]byte("432 - Header Fields Too Large"))
	case "444":
		w.WriteHeader(444)
		w.Write([]byte("444 - Connection Closed Without Response (Nginx Only)"))
	case "451":
		w.WriteHeader(http.StatusUnavailableForLegalReasons)
		w.Write([]byte("451 - Unavailable For Legal Reasons"))
	case "499":
		w.WriteHeader(499)
		w.Write([]byte("499 - Client Closed Request (Nginx Only)"))

	case "500":
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Internal Server Error"))
	case "501":
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte("501 - Not Implemented"))
	case "502":
		w.WriteHeader(http.StatusBadGateway)
		w.Write([]byte("502 - Bad Gateway"))
	case "503":
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write([]byte("503 - Service Unavailable"))
	case "504":
		w.WriteHeader(http.StatusGatewayTimeout)
		w.Write([]byte("504 - Gateway Timeout"))
	case "505":
		w.WriteHeader(http.StatusHTTPVersionNotSupported)
		w.Write([]byte("505 - HTTP Version Not Supported"))
	case "506":
		w.WriteHeader(http.StatusVariantAlsoNegotiates)
		w.Write([]byte("506 - Variant Also Negotiates"))
	case "507":
		w.WriteHeader(http.StatusInsufficientStorage)
		w.Write([]byte("507 - Insufficient Storage"))
	case "508":
		w.WriteHeader(http.StatusLoopDetected)
		w.Write([]byte("508 - Loop Detected"))
	case "510":
		w.WriteHeader(http.StatusNotExtended)
		w.Write([]byte("510 - Not Extended"))
	case "511":
		w.WriteHeader(http.StatusNetworkAuthenticationRequired)
		w.Write([]byte("511 - Network Authentication Required"))
	case "599":
		w.WriteHeader(599)
		w.Write([]byte("599 - Network Connection Timeout Error (Not Formal)"))
	}
}
